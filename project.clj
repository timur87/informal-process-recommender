(defproject informal-process-recommender "0.2.0-SNAPSHOT"
  :description "A recommender module to create informal process
  resource model recommendations"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [clj-time "0.12.0"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/data.zip "0.1.2"]
                 [org.eclipse.mylyn.github/org.eclipse.egit.github.core "2.1.5"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [org.springframework/spring-context "4.3.3.RELEASE"]
                 [org.json/json "20151123"]
                 [junit/junit "4.12"]
                 [tentacles "0.5.1"]
                 [clojure-csv/clojure-csv "2.0.2"]
                 [org.apache.directory.studio/org.apache.commons.codec "1.6"]]
  :plugins [[clj-jaxb/lein-xjc "0.2.0-SNAPSHOT"]]
  :xjc-plugin {:xjc-calls [{:xsd-file "src/main/resources/RIM-v0.1.xsd"}]}
  :source-paths ["src/main/clj"]
  :java-source-paths ["src/main/java" "src/test/java"]
  :resource-paths ["src/main/resources" "src/test/resources"])
