## informal-process-recommender
New location: https://bitbucket.org/timurhan7/informal-process-recommender
----
## Configuration and Usage
* To recreate the results run src/test/java/uni_stuttgart/ipsm/ipe_recommender/TTest.java after setting up the environment

The test will use the files stored in (src/test/resources) to create the relevance relationships and new informal process definitions.

For xsd definitions of the above classes please see [RIM-v0.1.xsd](src/main/resources/RIM-v0.1.xsd).

Here, we have an example where the input includes two informal proces instances, which will be analyzed together.

## What is informal-process-recommender?

A module which takes in one(or multiple) informal process definitions, analyzes, and interprets the interactions launched through IT services in the processes,
and, gives recommendations of resources, their capabilities.

----
## Install

1.Install [Leiningen](http://leiningen.org/#install).

2.`git clone` the [protocols repository](https://gitlab.com/timur87/protocols) , run`lein install` under the root folder to install all the classes
defined in XML schema into local Leiningen repository.
$ git clone https://gitlab.com/timur87/protocols.git

3.`git clone` this repository, run `lein install` under the root foler.
$ git clone https://gitlab.com/timur87/protocols.git

4. Launch TTest.java to regenerate the results

5.Get recommendations:

----
## Structure

	├─doc
	└─src
		├─main
		│  ├─clj
		│  ├─java
		│  │  └─uni_stuttgart
		│  │      └─ipsm
		│  │          └─ipe_recommender
		│  │              ├─analyzers			//retrieves interactions from resources
		│  │              │  ├─impl
		│  │              │  └─results			//interface of data object as input/output for analyzers
		│  │              │      └─impl
		│  │              ├─apis
		│  │              ├─relevance_mappers		//retrieves relationships/capabilities from interactions
		│  │              │  ├─impl
		│  │              │  └─results
		│  │              │      └─impl			//interface of data object as input/output for interpreters
		│  │              ├─process_generator   //generates informal process recommendations
		│  │              └─utils				//id mappers,xml file readers,etc
		│  │                  └─impl
		│  └─resources
		├─test
		│  ├─java
		│  │  └─uni_stuttgart
		│  │      └─ipsm
		│  │          └─ipe_recommender			//a test unit based on spring framework
		│  └─resources
		└─web
			└─META-INF

----
## License

Copyright © 2016 IAAS, University of Stuttgart

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
