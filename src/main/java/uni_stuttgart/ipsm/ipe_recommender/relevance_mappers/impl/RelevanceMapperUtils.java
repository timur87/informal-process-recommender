package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;

import java.util.List;

import javax.xml.namespace.QName;

import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import de.uni_stuttgart.iaas.ipsm.v0.TInteractionList;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevantResource;
import de.uni_stuttgart.iaas.ipsm.v0.TResourceRefs;
import de.uni_stuttgart.iaas.ipsm.v0.TSourceEntities;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevantCapability;

public class RelevanceMapperUtils {


    public static TRelevanceRelationship findRelevanceRelationshipForRelevantResource(QName relatedResourceDefinitionId, List<TRelevanceRelationship> relevanceRelationships){
        if(relevanceRelationships != null){
            for(TRelevanceRelationship relevanceRelationship : relevanceRelationships){
                if(relevanceRelationship.getRelevantCapabilityOrResource().getClass().equals(TRelevantResource.class)){
                    if(((TRelevantResource) relevanceRelationship.getRelevantCapabilityOrResource()).getRelevantResource().getNamespaceURI().equals(relatedResourceDefinitionId.getNamespaceURI()) &&
                       ((TRelevantResource) relevanceRelationship.getRelevantCapabilityOrResource()).getRelevantResource().getLocalPart().equals(relatedResourceDefinitionId.getLocalPart()))
                        return relevanceRelationship;
                }
            }
        }
        return null;
    }


    public static TRelevanceRelationship findRelevanceRelationshipForRelevantCapability(QName relevantCapabilityId, List<TRelevanceRelationship> relevanceRelationships){
        if(relevanceRelationships != null){
            for(TRelevanceRelationship relevanceRelationship : relevanceRelationships){

                if(relevanceRelationship.getRelevantCapabilityOrResource().getClass().equals(TRelevantCapability.class)){
                    if(((TRelevantCapability) relevanceRelationship.getRelevantCapabilityOrResource()).getRelevantCapability().getNamespaceURI().equals(relevantCapabilityId.getNamespaceURI()) &&
                       ((TRelevantCapability) relevanceRelationship.getRelevantCapabilityOrResource()).getRelevantCapability().getLocalPart().equals(relevantCapabilityId.getLocalPart()))
                        return relevanceRelationship;
                }
            }
        }
        return null;
    }

    public static TRelevanceRelationship createRelevanceRelationshipForRelevantResource(QName relatedResourceDefinitionId){
        TRelevantResource relevantResource = new TRelevantResource();
        relevantResource.setRelevantResource(relatedResourceDefinitionId);

        TRelevanceRelationship relevanceRelationship = new TRelevanceRelationship();
        relevanceRelationship.setRelevantCapabilityOrResource(relevantResource);


        TSourceEntities sourceEntities = new TSourceEntities();
        relevanceRelationship.setSourceEntities(sourceEntities);

        TInteractionList interactionList = new TInteractionList();
        TResourceRefs resourceRefs = new TResourceRefs();
        relevanceRelationship.getSourceEntities().setSourceInteractions(interactionList);
        relevanceRelationship.getSourceEntities().setSourceResources(resourceRefs);

        return relevanceRelationship;
    }


    public static TRelevanceRelationship createRelevanceRelationshipForRelevantCapability(QName relevantCapabilityId){
        TRelevantCapability relevantResource = new TRelevantCapability();
        relevantResource.setRelevantCapability(relevantCapabilityId);
        TRelevanceRelationship relevanceRelationship = new TRelevanceRelationship();
        relevanceRelationship.setRelevantCapabilityOrResource(relevantResource);

        TSourceEntities sourceEntities = new TSourceEntities();
        relevanceRelationship.setSourceEntities(sourceEntities);

        TInteractionList interactionList = new TInteractionList();
        relevanceRelationship.getSourceEntities().setSourceInteractions(interactionList);

        TResourceRefs resourceRefs = new TResourceRefs();
        relevanceRelationship.getSourceEntities().setSourceResources(resourceRefs);
        return relevanceRelationship;
    }

    public static TRelevanceRelationship createRelevanceRelationshipForRelevantResource(QName relatedResourceDefinitionId, double baseCorrelationCoefficient){
        TRelevanceRelationship relevanceRelationship = createRelevanceRelationshipForRelevantResource(relatedResourceDefinitionId);
        relevanceRelationship.setCorrelationCoefficient(baseCorrelationCoefficient);
        System.err.println("New relevance relationship is being created..." + relatedResourceDefinitionId);
        return relevanceRelationship;
    }


    public static TRelevanceRelationship createRelevanceRelationshipForRelevantCapability(QName relevantCapability, double baseCorrelationCoefficient){
        TRelevanceRelationship relevanceRelationship = createRelevanceRelationshipForRelevantCapability(relevantCapability);
        relevanceRelationship.setCorrelationCoefficient(baseCorrelationCoefficient);
        System.err.println("New relevance relationship is being created..." + relevantCapability);
        return relevanceRelationship;
    }


    public static TRelevanceRelationship createRelevanceRelationshipForRelevantResource(QName relatedResourceDefinitionId, double baseCorrelationCoefficient, TInteraction interaction){
        TRelevanceRelationship relevanceRelationship = createRelevanceRelationshipForRelevantResource(relatedResourceDefinitionId,baseCorrelationCoefficient);
        relevanceRelationship.getSourceEntities().getSourceInteractions().getInteraction().add(interaction);
        return relevanceRelationship;
    }


    public static boolean isInteractionAlreadyIncluded(TRelevanceRelationship relevanceRelationship, TInteraction interaction){
        for (TInteraction tempInteraction : relevanceRelationship.getSourceEntities().getSourceInteractions().getInteraction()){
            if (tempInteraction.getDigest().equals(interaction.getDigest())){
                return true;
            }
        }
        return false;
    }

    public static void addInteractionIntoRelevanceRelationshipSources(TRelevanceRelationship relevanceRelationship, TInteraction interaction){
        relevanceRelationship.getSourceEntities().getSourceInteractions().getInteraction().add(interaction);
    }

}
