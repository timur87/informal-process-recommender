package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;

import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.ICorrelationCoefficientCalculator;

@Service
public class CorrelationCoefficientCalculator implements ICorrelationCoefficientCalculator {


    /**
     *
     */
    public CorrelationCoefficientCalculator() {
    }

    private boolean controlRelevanceFactor(double relevanceFactor){

        if(-1 <= relevanceFactor && 1 >= relevanceFactor){
            return true;
        }
        else{
            return false;
        }

    }

    private boolean controlWeight(int weight){

        if(1 <= weight){
            return true;
        }
        else{
            return false;
        }

    }

    private boolean controlOldValue(double oldValue){

        if(-1 <= oldValue && 1 >= oldValue){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public double calculateCorrelationCoefficient(double oldValue, double relevanceFactor, int weight){
        if(controlWeight(weight) && controlOldValue(oldValue) && controlRelevanceFactor(relevanceFactor))
        {
            double expContent = Math.abs(1 - Math.abs(oldValue));
            double propotionalFraction = 1 - Math.abs(oldValue) / Math.exp(expContent);
            double updateCoefficient = relevanceFactor / weight;
            double newValue = oldValue + propotionalFraction * updateCoefficient;
            // Set upper and lower limits correlationcoefficient must be between including 1 and -1
            if(newValue >= 1){
                return 1;
            } else if (newValue <= -1){
                return -1;
            }

            return newValue;
        }
        else{
            return oldValue;
        }

    }

    @Override
    public TRelevanceRelationship updateCorrelationCoefficient(TRelevanceRelationship relevanceRelationship,double relevanceFactor, int distance) {
        double oldValue = relevanceRelationship.getCorrelationCoefficient();
        relevanceRelationship.setCorrelationCoefficient(calculateCorrelationCoefficient(oldValue, relevanceFactor, distance));
        return relevanceRelationship;
    }

}
