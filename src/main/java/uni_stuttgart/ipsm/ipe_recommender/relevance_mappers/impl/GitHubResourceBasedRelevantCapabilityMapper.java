package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevantResource;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl.RelevanceMapperUtils;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.ICorrelationCoefficientCalculator;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantCapabilityMapper;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;
import uni_stuttgart.ipsm.ipe_recommender.utils.ICapabilityReader;

@Service
public class GitHubResourceBasedRelevantCapabilityMapper implements IRelevantCapabilityMapper {
	
	private ICapabilityReader capabilityReader;
	private ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator;
	public final double BASE_CC_AND_RELEVANCE_FACTOR = 0.1;
	
	
	@Autowired
	public GitHubResourceBasedRelevantCapabilityMapper(ICapabilityReader capabilityReader, ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator) {
		this.setCapabilityReader(capabilityReader);
		this.setiCorrelationCoefficientCalculator(iCorrelationCoefficientCalculator);
	}
	
	@Override
	public IRelevanceResults interpretResult(IRelevanceResults interpretationResult) {
		
		/*Get the crawled list of relevance relationships*/
		List<TRelevanceRelationship> relevanceRelationships = interpretationResult.getRelevanceRelationships();
		
		/*Initialize the list to hold temporary relationships*/
		List<TRelevanceRelationship> tempRelationships = new ArrayList<TRelevanceRelationship>();
		
		/*Iterate through given relevance relationships
		 * for related resources*/
		for (TRelevanceRelationship trr : relevanceRelationships) {
			/*If the current relevance relationship
			 * contains a resource, look into the resource*/
			if (trr.getRelevantCapabilityOrResource().getClass().equals(TRelevantResource.class)) {
				/*Look for defined capability of this resource
				 * from input file*/
				QName resource = ((TRelevantResource) trr.getRelevantCapabilityOrResource()).getRelevantResource();
				
				List<QName> capabilityTypes = this.getCapabilityReader().findCapabilityTypesForNodeType(resource);
				// check and create new relevance relationships of found capabilities
				for (QName relevantCapabilityId : capabilityTypes) {
					TRelevanceRelationship relevanceRelationshipOfCapability = RelevanceMapperUtils.findRelevanceRelationshipForRelevantCapability(relevantCapabilityId, relevanceRelationships);
					// if it does not exist create new one
					if (relevanceRelationshipOfCapability == null) {
						relevanceRelationshipOfCapability = RelevanceMapperUtils.createRelevanceRelationshipForRelevantCapability(relevantCapabilityId, this.BASE_CC_AND_RELEVANCE_FACTOR);
						tempRelationships.add(relevanceRelationshipOfCapability);
					}
					// update the relevance factor
					this.getiCorrelationCoefficientCalculator().updateCorrelationCoefficient(relevanceRelationshipOfCapability, this.BASE_CC_AND_RELEVANCE_FACTOR, interpretationResult.getDistance());
				}
			}
			
		}
		// add all new ones to the list of found ones
		relevanceRelationships.addAll(tempRelationships);
		
		return interpretationResult;
	}
	
	/**
	 * @return the capabilityReader
	 */
	public ICapabilityReader getCapabilityReader() {
		return this.capabilityReader;
	}
	
	/**
	 * @param capabilityReader the capabilityReader to set
	 */
	public void setCapabilityReader(ICapabilityReader capabilityReader) {
		this.capabilityReader = capabilityReader;
	}
	
	/**
	 * @return the iCorrelationCoefficientCalculator
	 */
	public ICorrelationCoefficientCalculator getiCorrelationCoefficientCalculator() {
		return this.iCorrelationCoefficientCalculator;
	}
	
	/**
	 * @param iCorrelationCoefficientCalculator the
	 *            iCorrelationCoefficientCalculator to set
	 */
	public void setiCorrelationCoefficientCalculator(ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator) {
		this.iCorrelationCoefficientCalculator = iCorrelationCoefficientCalculator;
	}
	
}
