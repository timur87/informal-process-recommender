package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers;

import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;

public interface IRelevantCapabilityMapperForGit {

	/**
	 * Parse and interpret given result
	 *
	 * @param interpretationResult
	 * @return
	 */
	IRelevanceResults interpretResult(IRelevanceResults interpretationResult);

}
