package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results;

import java.util.List;



import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;


public interface IRelevanceResults {

	/**
	 * Return calculated IRelevanceResults
	 * @return
	 */
	IResourceAnalysisResult getResourceAnalysisResult();


	void setResourceAnalysisResult(IResourceAnalysisResult resourceAnalysisResult);


	List<TRelevanceRelationship> getRelevanceRelationships();

	int getDistance();

	void setDistance(int distance);

	List<TRelevanceRelationship> getOldRelevanceRelationships();


	}
