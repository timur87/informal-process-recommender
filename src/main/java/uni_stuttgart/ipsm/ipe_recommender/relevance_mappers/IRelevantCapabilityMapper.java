package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers;

import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;

public interface IRelevantCapabilityMapper extends IRelevantResourceMapper {

	/**
	 * Parse and interpret given result
	 *
	 * @param interpretationResult
	 * @return
	 */
	@Override
	IRelevanceResults interpretResult(IRelevanceResults interpretationResult);

}
