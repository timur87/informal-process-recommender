package uni_stuttgart.ipsm.ipe_recommender;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.json.JSONException;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TServiceTemplate;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptors;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.IResourceAnalyzer;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;
import uni_stuttgart.ipsm.ipe_recommender.apis.InformalProcessRecommenderApi;
import uni_stuttgart.ipsm.ipe_recommender.process_generator.IInformalProcessDefinitionsGenerator;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantCapabilityMapper;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantCapabilityMapperForGit;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantResourceMapper;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;
import uni_stuttgart.ipsm.ipe_recommender.utils.EntityAccessUtils;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

/**
 * Provides the main functionalities of this project by defining the method
 * getRecommendationForInformalProcessInstances which takes in a
 * TInformalProcessDefinition object and a list of TInformalProcessInstance
 * objects, and updates the former after analyzing the latter.
 */
public class InformalProcessRecommenderSpringBased implements InformalProcessRecommenderApi {

	private AnnotationConfigApplicationContext context = null;

	public IRelevantResourceMapper gitInterpreter;
	public IRelevantCapabilityMapperForGit gitCapaInterpreter;
	private TInformalProcessDefinition tInformalProcessDefinition;
	private IInformalProcessDataStoreAccess xmlFileReader;
	final int RESOURCE_DISTANCE = 2;


	/**
	 * Constructor
	 */
	public InformalProcessRecommenderSpringBased() {
		// this.scenarioName = fileName;
		this.setContext(new AnnotationConfigApplicationContext());
		Properties prop = new Properties();
		InputStream input = null;
		String[] packages = null;
		try {
			//            FileInputStream fileInputStream = new FileInputStream(Constants.CONFIG_FILE_NAME);
			input = this.getClass().getClassLoader().getResourceAsStream(Constants.CONFIG_FILE_NAME);
			prop.load(input);
			String listOfPackages = prop.getProperty(Constants.CONFIG_VAR_IMPLEMENTATION_PACKAGES);
			packages = listOfPackages.split(" ");
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.getContext().scan(packages);
		this.getContext().refresh();
		this.setXmlFileReader(this.getContext().getBean(IInformalProcessDataStoreAccess.class));

	}

	/**
	 * Push the resource instance through all analyzers one by one , updating
	 * the analysisResult each time.
	 *
	 * @param resourceInstance
	 * @param nodeType
	 * @param analysisResult
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws MalformedURLException
	 * @throws JSONException
	 * @throws IOException
	 * @throws JAXBException {@inheritDoc}
	 */
	private IResourceAnalysisResult analyzeResourceWithAllAnalyzers(TInstanceDescriptor resourceInstance, QName nodeType, IResourceAnalysisResult analysisResult) {
		// Configure analysis result
		analysisResult.setTargetResourceInstance(resourceInstance);
		analysisResult.setTargetResourceDefinitionId(nodeType);
		// Get resource analyzers
		Map<String, IResourceAnalyzer> analyzers = this.getContext().getBeansOfType(IResourceAnalyzer.class);
		// Iterate through resource analyzers
		for (IResourceAnalyzer analyzer : analyzers.values()) {
			analysisResult = analyzer.analyzeResource(analysisResult);
		}
		// return the results
		return analysisResult;
	}

	/**
	 *
	 * @param informalProcessDefinition, informalProcessInstances Get an
	 *            informal process instance analyze its resource instances using
	 *            interaction analyzers and return a proposed resource model
	 *            based on the analysis results.
	 * @return Recommendation model {@inheritDoc}
	 * @throws JAXBException
	 * @throws IOException
	 * @throws JSONException
	 * @throws MalformedURLException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@Override
	public TInformalProcessDefinition getRecommendationForInformalProcessInstances(TInformalProcessDefinition informalProcessDefinition, List<TInformalProcessDefinition> informalProcessInstances) {
		List<TRelevanceRelationship> listOfRelevanceRelationships = this.getRelevanceRelationshipsForInformalProcessInstances(informalProcessDefinition, informalProcessInstances);
		IInformalProcessDefinitionsGenerator processGenerator = this.getContext().getBean(IInformalProcessDefinitionsGenerator.class);

		this.tInformalProcessDefinition = processGenerator.generateInformalProcessDefinition(informalProcessDefinition, listOfRelevanceRelationships);
		return this.tInformalProcessDefinition;

	}

	/**
	 *
	 * @param informalProcessDefinition, informalProcessInstances Get an
	 *            informal process instance analyze its resource instances using
	 *            interaction analyzers and return a proposed resource model
	 *            based on the analysis results.
	 * @return Recommendation model {@inheritDoc}
	 * @throws JAXBException
	 * @throws IOException
	 * @throws JSONException
	 * @throws MalformedURLException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@Override
	public TInformalProcessDefinition getRecommendationForInformalProcessInstancesUsingRelavanceRelationships(TInformalProcessDefinition informalProcessDefinition, List<TRelevanceRelationship> listOfRelevanceRelationships) {

		IInformalProcessDefinitionsGenerator processGenerator = this.getContext().getBean(IInformalProcessDefinitionsGenerator.class);

		this.tInformalProcessDefinition = processGenerator.generateInformalProcessDefinition(informalProcessDefinition, listOfRelevanceRelationships);
		return this.tInformalProcessDefinition;

	}

	/**
	 * Pass the IRelevantCapabilityResults object through all implemented
	 * capability interpreters.
	 *
	 * @param relevantCapabilityResults
	 * @return {@inheritDoc}
	 * @throws JAXBException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	private IRelevanceResults interpretInteractionsForRelevantCapabilities(IRelevanceResults relevanceResults) {

		Map<String, IRelevantCapabilityMapper> mappers = this.getContext().getBeansOfType(IRelevantCapabilityMapper.class);
		for (IRelevantCapabilityMapper mapper : mappers.values()) {
			relevanceResults = mapper.interpretResult(relevanceResults);
		}
		return relevanceResults;
	}

	/**
	 * Pass the IRelevanceResults object through all implemented relationship
	 * interpreters.
	 *
	 * @param relevanceResult
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws JAXBException {@inheritDoc}
	 */
	private IRelevanceResults interpretInteractionsForRelevanceRelationships(IRelevanceResults relevanceResult) {
		Map<String, IRelevantResourceMapper> mappers = this.getContext().getBeansOfType(IRelevantResourceMapper.class);
		for (IRelevantResourceMapper mapper : mappers.values()) {
			relevanceResult = mapper.interpretResult(relevanceResult);
		}
		return relevanceResult;
	}

	/**
	 * Get all resource instances from the declared xml files.
	 *
	 * @param informalProcessDefinitions
	 * @return
	 * @throws JAXBException {@inheritDoc}
	 */
	private Map<TInstanceDescriptor, QName> getAllResourceInstances(List<TInformalProcessDefinition> informalProcessDefinitions) {

		Map<TInstanceDescriptor, QName> instanceMap = new HashMap<TInstanceDescriptor, QName>();
		// Check all given process definitions

		for (TInformalProcessDefinition tpi : informalProcessDefinitions) {
			//  check resource models of each informal process definition
			QName serviceTemplateRef = tpi.getResourceModel().getRef();

			for (TExtensibleElements element : tpi.getResourceModel().getDefinitions().getServiceTemplateOrNodeTypeOrNodeTypeImplementation()) {
				// check service templates
				// If resource ref has been specified use that to find the right service template, there can be multiple service templates defined
				if (element.getClass().equals(TServiceTemplate.class) && (serviceTemplateRef == null || ((serviceTemplateRef.getNamespaceURI() == ((TServiceTemplate) element).getTargetNamespace()) && (serviceTemplateRef.getLocalPart() == ((TServiceTemplate) element).getName())))) {
					for (TEntityTemplate tet : ((TServiceTemplate) element).getTopologyTemplate().getNodeTemplateOrRelationshipTemplate())
						// check if the type is a "resource model element"
						if (TNodeTemplate.class.isInstance(tet)) {

							// there is only need to check for duplication
							// where there are multiple input IP instances
							// Get node template which may be containing multiple intstance descriptors
							for (Object obj : ((TNodeTemplate) tet).getAny()) {
								// check all instance descriptors if there are some
								if (JAXBElement.class.isInstance(obj) && TInstanceDescriptors.class.isInstance(((JAXBElement) obj).getValue())) {

									TInstanceDescriptors instanceDescriptors = ((JAXBElement<TInstanceDescriptors>) obj).getValue();
									for (TInstanceDescriptor instance : instanceDescriptors.getInstanceDescriptor()) {
										// Add if it's not already there
										if (!EntityAccessUtils.isInstanceAlreadyInCollection(instance, instanceMap.keySet())) {
											instanceMap.put(instance, ((TNodeTemplate) tet).getType());
										}

									}

								}
							}
						}
				}
			}
		}
		return instanceMap;
	}

	public AnnotationConfigApplicationContext getContext() {
		return this.context;
	}

	public void setContext(AnnotationConfigApplicationContext context) {
		this.context = context;
	}

	/**
	 * @return the xmlFileReader
	 */
	public IInformalProcessDataStoreAccess getXmlFileReader() {
		return this.xmlFileReader;
	}

	/**
	 * @param xmlFileReader the xmlFileReader to set
	 */
	public void setXmlFileReader(IInformalProcessDataStoreAccess xmlFileReader) {
		this.xmlFileReader = xmlFileReader;
	}

	private void updateInputResourcesBasedOnRelevanceRelationships(IResourceAnalysisResult resourceAnalysisResults) {
		Map<TInstanceDescriptor, QName> newInstanceDescriptors = new HashMap<TInstanceDescriptor, QName>();
		for (TInteraction interaction : resourceAnalysisResults.getInteractions()) {
			QName typeOfInsatnce = this.getXmlFileReader().getResourceDefinitionBasedOnInstanceUri(interaction.getIdentifiedResource().getInstanceURI());

			if (typeOfInsatnce == null) {
				System.err.println("Resource type could not be identified for " + interaction.getIdentifiedResource().getInstanceURI());
				continue;
			}
			newInstanceDescriptors.put(interaction.getIdentifiedResource(), typeOfInsatnce);
		}
		resourceAnalysisResults.setInteractions(new ArrayList<TInteraction>());
		resourceAnalysisResults.setInputResourceInstances(newInstanceDescriptors);
	}

	/**
	 * @param targetInformalProcessDefinition
	 * @param relevantProcessDefinitions
	 * @return
	 */
	@Override
	public List<TRelevanceRelationship> getRelevanceRelationshipsForInformalProcessInstances(TInformalProcessDefinition targetInformalProcessDefinition, List<TInformalProcessDefinition> relevantProcessDefinitions) {
		if (targetInformalProcessDefinition == null) {
			// Add a log library
			System.err.println("Informal process definition cannot be null");
			return null;
		}
		// Analysis results passed between iterations, between resource analyzers
		IResourceAnalysisResult analysisResult = this.getContext().getBean(IResourceAnalysisResult.class);
		// relevance results passed between iterations, between relevance mappers
		IRelevanceResults relevanceResult = this.getContext().getBean(IRelevanceResults.class);
		// Analyze more levels iteratively
		// Get initial resources to be analyzed based on documented resources
		List<TInformalProcessDefinition> completeListofinformalProcessDefinitions = new ArrayList<TInformalProcessDefinition>();
		completeListofinformalProcessDefinitions.add(targetInformalProcessDefinition);
		if (relevantProcessDefinitions != null) {
			completeListofinformalProcessDefinitions.addAll(relevantProcessDefinitions);
		}
		Map<TInstanceDescriptor, QName> resourceInstances = this.getAllResourceInstances(completeListofinformalProcessDefinitions);
		//Get an instance of the IResourceAnalysisResult class
		// for the analysis results
		analysisResult.setInputResourceInstances(resourceInstances);
		analysisResult.setInteractions(new ArrayList<TInteraction>());
		// Iteratively collect interactions
		for (int currentDistance = 1; currentDistance <= this.RESOURCE_DISTANCE; currentDistance++) {

			// Analyze each resource instance with all the analyzers at hand,
			// the analysis result will be updated at each analysis.
			// After this step, we get all the interactions of this iteration
			for (TInstanceDescriptor instanceDescriptor : analysisResult.getInputResources().keySet()) {
				analysisResult = this.analyzeResourceWithAllAnalyzers(instanceDescriptor, analysisResult.getInputResources().get(instanceDescriptor), analysisResult);
			}
			//Interpret the interactions for relevance relationships.
			// After this stage we have a list of found relevance relationships
			relevanceResult.setResourceAnalysisResult(analysisResult);
			relevanceResult.setDistance(currentDistance);
			relevanceResult = this.interpretInteractionsForRelevanceRelationships(relevanceResult);
			// Interpret the interactions for relevance capabilities.
			// After this stage we have a list of relevance capabilities
			relevanceResult = this.interpretInteractionsForRelevantCapabilities(relevanceResult);
			this.updateInputResourcesBasedOnRelevanceRelationships(analysisResult);

		}
		// Get all resources

		return relevanceResult.getRelevanceRelationships();
	}

}
