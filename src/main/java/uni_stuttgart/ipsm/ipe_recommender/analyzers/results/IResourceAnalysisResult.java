package uni_stuttgart.ipsm.ipe_recommender.analyzers.results;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;

/**
 * Each interaction analysis inputs a processed or an empty analysis result.
 * This interface is a container of a such type
 */
public interface IResourceAnalysisResult {

    /**
     * Returns the interactions crawled from this resource.
     *
     * @return List<TInteraction>
     */
    List<TInteraction> getInteractions();

    /**
     * Sets interactions.
     *
     * @param interactions {@inheritDoc}
     */
    void setInteractions(List<TInteraction> interactions);

    TInstanceDescriptor getTargetResourceInstance();

    void setTargetResourceInstance(TInstanceDescriptor targetResource);

    QName getTargetResourceDefinitionId();

    void setTargetResourceDefinitionId(QName definitionId);

    boolean addInteraction(TInteraction interaction);

    void setInputResourceInstances(Map<TInstanceDescriptor,QName> inputInstancesAndTheirTypes);

    Map<TInstanceDescriptor,QName> getInputResources();


}
