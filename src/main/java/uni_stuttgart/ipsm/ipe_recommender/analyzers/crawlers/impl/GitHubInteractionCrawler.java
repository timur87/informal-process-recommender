package uni_stuttgart.ipsm.ipe_recommender.analyzers.crawlers.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.eclipse.egit.github.core.User;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.client.GsonUtils;
import org.eclipse.egit.github.core.client.PageIterator;
import org.eclipse.egit.github.core.event.Event;
import org.eclipse.egit.github.core.service.EventService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.egit.github.core.service.UserService;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TContent;
import de.uni_stuttgart.iaas.ipsm.v0.TEntityIdentity;
import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.crawlers.IGitHubInteractionCrawler;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.impl.GitConstants;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
public class GitHubInteractionCrawler implements IGitHubInteractionCrawler {
	
	public static final QName TARGET_TYPE = new QName("http://www.uni-stuttgart.de/ipsm/resources/it-resources", "GitHub Repository");
	private final static String USER_NAME = "songmozi@163.com";
	private final static String PASSWORD = "ipr2016";
	
	
	/**
	 *
	 * Check if the given resource definition id aka node type is supported git
	 * type
	 *
	 * @param resourceDefinitionId
	 * @return
	 */
	@Override
	public boolean isSupportedResourceDefinitionId(QName resourceDefinitionId) {
		return (resourceDefinitionId.equals(TARGET_TYPE));
	}
	
	private String createInteractionDigest(TInteraction interaction) {
		try {
			// create a digest obj
			MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
			// digest the content of the string
			/*getting the string out of interaction content as a json object */
			Object content = interaction.getContent().getAny();
			if (content != null) {
				//TODO check if this really works
				msgDigest.update((interaction.getAnalyzedResource() + interaction.getIdentifiedResource().toString() + content.toString()).getBytes());
				BigInteger resultingInteger = new BigInteger(msgDigest.digest());
				return resultingInteger.toString();
			} else {
				System.out.println("Undefined content of interaction, or type of content is not JSONObject. Interaction not added. Random id will be used");
				return UUID.randomUUID().toString();
			}
						
		} catch (NoSuchAlgorithmException e) {
			System.out.println("NoSuchAlgorithmException in addInteraction!");
			e.printStackTrace();
		}
		// random will be used intead of hash
		return UUID.randomUUID().toString();
	}
	
	/**
	 * Genearte a new interaction with the event actor as source and the crawled
	 * github repository as target, the event type being the action, and the
	 * event as content. The cardinality of interaction does not matter for each
	 * pair in an interaction create a new interaction
	 *
	 * @param event
	 * @param targetNamespace
	 * @return {@inheritDoc}
	 */
	private TInteraction newInteraction(Event event, TInstanceDescriptor crawledResource, boolean isAnalyzedResource, IInformalProcessDataStoreAccess xmlFileNameReader) {
		TInteraction interaction = new TInteraction();
		String humanName = event.getActor().getUrl();
		String type = event.getType();
		/*get the verb part of interaction*/
		String action = Character.toLowerCase(type.charAt(0)) + type.substring(1, type.length() - 5);
		TContent content = new TContent();
		TInstanceDescriptor identifiedInstance = xmlFileNameReader.getInstanceDescriptorBasedOnInstanceUri(humanName);

		if (identifiedInstance == null) {

			GitHubClient githubClient = new GitHubClient();
			// Set credentials
			// TODO change with a config service
			String userType = event.getActor().getLogin();
			githubClient.setCredentials(USER_NAME, PASSWORD);
			UserService userService = new UserService(githubClient);
			try {
				User relevantUser = userService.getUser(event.getActor().getLogin());
				if (relevantUser.getName() != null) {
					userType = relevantUser.getName();
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.err.println("<NodeTemplate id=\"" + UUID.randomUUID().toString() + "\" name=\"Software Developer\" type=\"rs:" + userType + "\">\n<ipsm:InstanceDescriptors><ipsm:InstanceDescriptor instanceState=\"allocated\"  instanceURI=\"" + humanName + "\"/>\n</ipsm:InstanceDescriptors>\n</NodeTemplate>");
			
			return null;
		}
		
		if (!isAnalyzedResource) {
			interaction.setAnalyzedResource(identifiedInstance);
			interaction.setIdentifiedResource(crawledResource);
		} else {
			interaction.setIdentifiedResource(identifiedInstance);
			interaction.setAnalyzedResource(crawledResource);
		}
		content.setAny(new JAXBElement<String>(new QName(GitConstants.GIT_INTERACTION_NS, GitConstants.GIT_INTERACTION_NAME), String.class, "<![CDATA[" + GsonUtils.toJson(event) + "]]>"));
		interaction.setAction(action);
		interaction.setContent(content);
		interaction.setDigest(this.createInteractionDigest(interaction));
		TEntityIdentity entityIdentity = new TEntityIdentity();
		interaction.setEntityIdentity(entityIdentity);
		interaction.getEntityIdentity().setTargetNamespace(GitConstants.GIT_INTERACTION_NS);
		interaction.getEntityIdentity().setName(GitConstants.GIT_INTERACTION_NAME);
		
		return interaction;
	}
	
	private String getRepoNameFromInstanceUri(TInstanceDescriptor resourceInstance) {
		String[] segments = resourceInstance.getInstanceURI().split("/");
		
		if (segments.length > 2) {
			return segments[segments.length - 1];
		} else {
			
			return null;
		}
	}
	
	private String getRepoOwnerFromInstanceUri(TInstanceDescriptor resourceInstance) {
		String[] segments = resourceInstance.getInstanceURI().split("/");
		
		if (segments.length > 2) {
			return segments[segments.length - 2];
			
		} else {
			
			return null;
		}
	}
	
	private List<Event> getRepositoryEvents(String repoOwner, String repoName) {
		
		// Init services
		GitHubClient githubClient = new GitHubClient();
		// Set credentials
		// TODO change with a config service
		
		githubClient.setCredentials(USER_NAME, PASSWORD);
		
		RepositoryService repositoryService = new RepositoryService(githubClient);
		EventService eventService = new EventService(githubClient);
		List<Event> fetchedEvents = new ArrayList<Event>();
		
		PageIterator<Event> pageIterator = null;
		try {
			pageIterator = eventService.pageEvents(repositoryService.getRepository(repoOwner, repoName));
		} catch (IOException e) {
			e.printStackTrace();
			return fetchedEvents;
		}
		
		while (pageIterator.hasNext()) {
			for (Event event : pageIterator.next()) {
				fetchedEvents.add(event);
			}
			
		}
		return fetchedEvents;
	}
	
	/**
	 *
	 *
	 * @param toBeCrawledInstance
	 * @param idMapper
	 * @param resourcesToBeIgnored
	 * @param isAnalyzedResource Used to set the analyzed resource in the
	 *            created interaction only interactions of existing elements in
	 *            the modeling environment will be stored. Therefore the list of
	 *            TNodeTemplates are searched
	 * @return
	 */
	@Override
	public List<TInteraction> crawlForInteractions(TInstanceDescriptor toBeCrawledInstance, boolean isAnalyzedResource, IInformalProcessDataStoreAccess xmlFileNameReader) {
		// prepare repo settings
		String repoName = this.getRepoNameFromInstanceUri(toBeCrawledInstance);
		String repoOwner = this.getRepoOwnerFromInstanceUri(toBeCrawledInstance);
		List<TInteraction> returnList = new ArrayList<TInteraction>();
		if (repoName == null || repoOwner == null) {
			System.err.println("Malformed repoName and repoOwner: " + repoName + " " + repoOwner);
			return returnList;
		}
		
		List<Event> eventList = this.getRepositoryEvents(repoOwner, repoName);
		for (Event event : eventList) {
			TInteraction newInteraction = this.newInteraction(event, toBeCrawledInstance, isAnalyzedResource, xmlFileNameReader);
			if (newInteraction != null) {
				returnList.add(newInteraction);
			}
		}
		
		return returnList;
		
	}

}
