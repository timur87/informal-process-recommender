package uni_stuttgart.ipsm.ipe_recommender.analyzers;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import org.json.JSONException;

import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;

/**
 * A generic interface to define analyzers
 */
public interface IResourceAnalyzer {
	
	/**
	 * Analyzes the target resource stored in the
	 * {@link IResourceAnalysisResult} if possible and returns either an updated
	 * or non updated result. These results are then analyzed in interpreters.
	 * 
	 * @param analysisResult
	 * @return {@inheritDoc}
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws JSONException
	 * @throws MalformedURLException
	 * @throws JAXBException
	 */
	IResourceAnalysisResult analyzeResource(IResourceAnalysisResult analysisResult);
	
}
