package uni_stuttgart.ipsm.ipe_recommender.analyzers.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.IResourceAnalyzer;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.crawlers.IGitHubInteractionCrawler;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

/**
 * Crawls the events of a repository and records all interactions.
 */
@Service
public class GitHubRepositoryAnalyzer implements IResourceAnalyzer {

	private List<TInteraction> interactions;
	private Map<TInstanceDescriptor, QName> resourcesToBeIgnored;
	private IInformalProcessDataStoreAccess iXmlFileNameReader;
	private IGitHubInteractionCrawler interactionCrawler;
	
	
	/**
	 * Constructor
	 */
	@Autowired
	public GitHubRepositoryAnalyzer(IInformalProcessDataStoreAccess iXmlFileNameReader, IGitHubInteractionCrawler interactionCrawler) {
		this.setInteractionCrawler(interactionCrawler);
		this.setInteractions(new ArrayList<TInteraction>());
		this.setiXmlFileNameReader(iXmlFileNameReader);
	}

	/**
	 * Getter for this.interactions
	 *
	 * @return {@inheritDoc}
	 */
	public List<TInteraction> getInteractions() {
		return this.interactions;
	}

	/**
	 * @param interactions the interactions to set
	 */
	public void setInteractions(List<TInteraction> interactions) {
		this.interactions = interactions;
	}

	/**
	 * @return the resourcesToBeIgnored
	 */
	public Map<TInstanceDescriptor, QName> getResourcesToBeIgnored() {
		return this.resourcesToBeIgnored;
	}

	/**
	 * @param resourcesToBeIgnored the resourcesToBeIgnored to set
	 */
	public void setResourcesToBeIgnored(Map<TInstanceDescriptor, QName> resourcesToBeIgnored) {
		this.resourcesToBeIgnored = resourcesToBeIgnored;
	}

	/**
	 * @return the iXmlFileNameReader
	 */
	public IInformalProcessDataStoreAccess getiXmlFileNameReader() {
		return this.iXmlFileNameReader;
	}

	/**
	 * @param iXmlFileNameReader the iXmlFileNameReader to set
	 */
	public void setiXmlFileNameReader(IInformalProcessDataStoreAccess iXmlFileNameReader) {
		this.iXmlFileNameReader = iXmlFileNameReader;
	}

	@Override
	/**
	 * Analysis the given resource instance and update the analysisResult.
	 */
	public IResourceAnalysisResult analyzeResource(IResourceAnalysisResult analysisResult) {
		// get the target resource instance from analysis result
		TInstanceDescriptor targetResourceInstance = analysisResult.getTargetResourceInstance();
		QName targetResourceDefId = analysisResult.getTargetResourceDefinitionId();

		// If target instance is not github repo
		if (targetResourceInstance == null || !targetResourceDefId.equals(GitConstants.TARGET_TYPE)) {
			return analysisResult;
		}

		// initialize interactions for crawling

		analysisResult.getInteractions().addAll(this.getInteractionCrawler().crawlForInteractions(targetResourceInstance, true, this.getiXmlFileNameReader()));

		return analysisResult;
	}
	
	public IGitHubInteractionCrawler getInteractionCrawler() {
		return this.interactionCrawler;
	}
	
	public void setInteractionCrawler(IGitHubInteractionCrawler interactionCrawler) {
		this.interactionCrawler = interactionCrawler;
	}

}
