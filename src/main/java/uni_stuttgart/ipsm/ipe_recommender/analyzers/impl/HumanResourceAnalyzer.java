package uni_stuttgart.ipsm.ipe_recommender.analyzers.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.IResourceAnalyzer;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.crawlers.IGitHubInteractionCrawler;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
/**
 * This analyzer class is based on the input .xml file which specifies the
 * additional github repositories to be crawled. These repostiories will be
 * looked up, and for each interaction found, if the interaction source name is
 * equal to the human being analyzed, then this interaction will be added to the
 * interactions list of the analysis result.
 *
 */
public class HumanResourceAnalyzer implements IResourceAnalyzer {

	public final String gitAPI = "https://api.github.com/";
	public final int GITHUB_PAGELIMIT = 10;
	private List<TInteraction> interactions;
	public static final String TARGET_TYPE_DOMAIN = "http://www.uni-stuttgart.de/ipsm/resources/human-resources";
	private List<TInstanceDescriptor> additionalGitInstances;
	//private TNodeTemplate cWikiInstance;
	private TInstanceDescriptor humanResourceInstance;
	private IInformalProcessDataStoreAccess xmlFileNameReader;
	private IGitHubInteractionCrawler interactionCrawler;


	/**
	 * Constructor
	 */
	@Autowired
	public HumanResourceAnalyzer(IInformalProcessDataStoreAccess xmlFileNameReader, IGitHubInteractionCrawler interactionCrawler) {

		this.setInteractionCrawler(interactionCrawler);
		this.setXmlFileNameReader(xmlFileNameReader);
		this.interactions = new ArrayList<TInteraction>();
	}

	/**
	 * Returns a boolean indicating whether the interaction source name is equal
	 * to the human name.
	 *
	 * @param i
	 * @return {@inheritDoc}
	 */
	private boolean interactionIsRelatedToInputHuman(TInteraction i) {
		if (i.getIdentifiedResource().equals(this.humanResourceInstance.getInstanceURI())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the additional github instances to be crawled.
	 *
	 * @return
	 * @throws JAXBException {@inheritDoc}
	 */
	private List<TInstanceDescriptor> getAdditionalInstances() {
		return this.getXmlFileNameReader().getInstanceDescriptorsOfAResourceDefintion(GitConstants.TARGET_TYPE);
	}

	/**
	 * Getter for this.interactions.
	 *
	 * @return {@inheritDoc}
	 */
	public List<TInteraction> getInteractions() {
		return this.interactions;
	}

	/**
	 * @param interactions the interactions to set
	 */
	public void setInteractions(List<TInteraction> interactions) {
		this.interactions = interactions;
	}

	/**
	 * @return the xmlFileNameReader
	 */
	public IInformalProcessDataStoreAccess getXmlFileNameReader() {
		return this.xmlFileNameReader;
	}

	/**
	 * @param xmlFileNameReader the xmlFileNameReader to set
	 */
	public void setXmlFileNameReader(IInformalProcessDataStoreAccess xmlFileNameReader) {
		this.xmlFileNameReader = xmlFileNameReader;
	}

	/**
	 * Crawl the additional github repositories for a certain human being and
	 * record the interactions originating from this human.
	 */
	@Override
	public IResourceAnalysisResult analyzeResource(IResourceAnalysisResult analysisResult) {
		// get the target resource instance from analysis result
		TInstanceDescriptor targetResourceInstance = analysisResult.getTargetResourceInstance();
		QName targetResourceDefId = analysisResult.getTargetResourceDefinitionId();
		
		// If target instance is not a human resource return, here we
		// check only the URI to understand the domain of the
		// resources but we do not check the name as there can be
		// humans with different names and listing all of them is not
		// possible
		if (targetResourceInstance == null || !targetResourceDefId.getNamespaceURI().equals(TARGET_TYPE_DOMAIN)) {
			return analysisResult;
		}
		
		/*record the human resource*/
		this.humanResourceInstance = targetResourceInstance;
		
		// this.inputResources = analysisResult.getInputResources();
		
		/*set up idMapper*/
		
		// get additional git instance
		this.additionalGitInstances = this.getAdditionalInstances();
		
		/*initialize interactions for crawling*/
		this.interactions = new ArrayList<TInteraction>();
		/*call the crawling function for interactions*/
		for (TInstanceDescriptor git : this.additionalGitInstances) {
			this.setInteractions(this.getInteractionCrawler().crawlForInteractions(git, false, this.getXmlFileNameReader()));
		}
		
		// set the interactions from crawling to analysisResult
		if (this.getInteractions() != null) {
			for (TInteraction i : this.getInteractions()) {
				if (this.interactionIsRelatedToInputHuman(i)) {
					analysisResult.addInteraction(i);
				}
			}
		} else
			System.out.println("HumanResourceAnalyzer not used to crawl this resource instance.");
		return analysisResult;
	}

	public IGitHubInteractionCrawler getInteractionCrawler() {
		return this.interactionCrawler;
	}

	public void setInteractionCrawler(IGitHubInteractionCrawler interactionCrawler) {
		this.interactionCrawler = interactionCrawler;
	}

}
