package uni_stuttgart.ipsm.ipe_recommender.analyzers.impl;

import javax.xml.namespace.QName;

/**
 *
 *
 * @author timur
 * @author
 * @version
 */
public class GitConstants {
	
	public static final QName TARGET_TYPE = new QName("http://www.uni-stuttgart.de/ipsm/resources/it-resources", "GitHub Repository");
	public static final String GIT_INTERACTION_NS = "http://www.uni-stuttgart.de/ipsm/interactions/";
	public static final String GIT_INTERACTION_NAME = "Git Interaction";
	public static final String GIT_INTERACTION_TAG = "GitInteraction";;
	
}
