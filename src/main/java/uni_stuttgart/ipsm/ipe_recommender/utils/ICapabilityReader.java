package uni_stuttgart.ipsm.ipe_recommender.utils;

import java.util.List;

import javax.xml.namespace.QName;

/**
 * This interface defines classes which reads documented capabilities
 * of resources from an .xml file.
 */
public interface ICapabilityReader {

	List<QName> findCapabilityTypesForNodeType(QName nodeType);

}
