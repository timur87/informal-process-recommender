package uni_stuttgart.ipsm.ipe_recommender.utils.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptors;
import de.uni_stuttgart.resource.InteractionMappings;
import de.uni_stuttgart.resource.ObjectFactory;
import de.uni_stuttgart.resource.TAvailableCapabilityDefinitions;
import de.uni_stuttgart.resource.TAvailableResourceDefinitions;
import de.uni_stuttgart.resource.TAvailableResourceModelElements;
import de.uni_stuttgart.resource.TOrganizationalDefinitions;
import uni_stuttgart.ipsm.ipe_recommender.Constants;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
public class InformalProcessDataStoreAccess implements IInformalProcessDataStoreAccess {
	
	public static final String PROP_NAME_IMPLEMENTATION_PACKAGES = "implementationPackages";
	public static final String PROP_NAME_RESORCE_ID_MAPPINGS = "idMappingForHumans";
	public static final String PROP_NAME_ADDITIONAL_GIT_REPOSITORIES = "additionalGitHubRepositoriesToBeCrawled";
	public static final String PROP_NAME_INTERACTION_MAPPINGS = "interactionToRelationshipAndCapabilityMappings";
	public static final String PROP_NAME_CAPABILITY_DEFINITIONS = "capabilityDefinitions";
	public static final String PROP_NAME_ORGANIZATIONAL_DEFINITIONS = "organizationalDefinitions";
	
	Properties prop = new Properties();
	
	private Unmarshaller unmarshaller;
	
	private TOrganizationalDefinitions organizationalDefinitions;
	
	
	public InformalProcessDataStoreAccess() {
		InputStream input = null;
		try {
			input = this.getClass().getClassLoader().getResourceAsStream(Constants.CONFIG_FILE_NAME);
			this.setUnmarshaller(JAXBContext.newInstance(ObjectFactory.class, de.uni_stuttgart.iaas.ipsm.v0.ObjectFactory.class).createUnmarshaller());
			this.prop.load(input);
			String fileName = this.prop.getProperty(PROP_NAME_ORGANIZATIONAL_DEFINITIONS);
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
			JAXBElement<TOrganizationalDefinitions> organizationalDefinitionsJaxbElement = (JAXBElement<TOrganizationalDefinitions>) this.getUnmarshaller().unmarshal(is);
			this.setOrganizationalDefinitions(organizationalDefinitionsJaxbElement.getValue());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			this.unmarshaller = null;
		} catch (IOException e) {
			e.printStackTrace();
			this.unmarshaller = null;
		} catch (JAXBException e) {
			e.printStackTrace();
			this.unmarshaller = null;
		}
	}
	
	@Override
	public TAvailableResourceDefinitions getAvailableResourceDefinitions() {
		return this.getOrganizationalDefinitions().getResourceDefinitions();
	}
	
	@Override
	public TAvailableCapabilityDefinitions getAvailableCapabilityDefinitions() {
		return this.getOrganizationalDefinitions().getCapabilityTypes();
	}
	
	@Override
	public TAvailableResourceModelElements getAdditionalGitHubRepositoriesToBeCrawled() {
		return this.getOrganizationalDefinitions().getResourceModelElements();
	}
	
	@Override
	public InteractionMappings getInteractionToRelationshipAndCapabilityMappings() {
		String fileName = this.prop.getProperty(PROP_NAME_INTERACTION_MAPPINGS);
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
		try {
			JAXBElement<InteractionMappings> interactionMappingsJaxBElement = (JAXBElement<InteractionMappings>) this.getUnmarshaller().unmarshal(is);
			InteractionMappings resourceMappings = interactionMappingsJaxBElement.getValue();
			return resourceMappings;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param organizationalDefinitions the organizationalDefinitions to set
	 */
	public void setOrganizationalDefinitions(TOrganizationalDefinitions organizationalDefinitions) {
		this.organizationalDefinitions = organizationalDefinitions;
	}
	
	/**
	 * @param organizationalDefinitions the organizationalDefinitions to set
	 */
	@Override
	public TOrganizationalDefinitions getOrganizationalDefinitions() {
		return this.organizationalDefinitions;
	}
	
	/**
	 * @return the prop
	 */
	public Properties getProp() {
		return this.prop;
	}
	
	/**
	 * @param prop the prop to set
	 */
	public void setProp(Properties prop) {
		this.prop = prop;
	}
	
	/**
	 * @return the unmarshaller
	 */
	public Unmarshaller getUnmarshaller() {
		return this.unmarshaller;
	}
	
	/**
	 * @param unmarshaller the unmarshaller to set
	 */
	public void setUnmarshaller(Unmarshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}
	
	/**
	 *
	 * Find the type of instance by iterating all initialized elements
	 * 
	 * @param instanceUri
	 * @return
	 */
	@Override
	public TInstanceDescriptor getInstanceDescriptorBasedOnInstanceUri(String instanceUri) {
		
		for (TNodeTemplate nodeTemplate : this.getAdditionalGitHubRepositoriesToBeCrawled().getNodeTemplate()) {
			for (Object anyObj : nodeTemplate.getAny()) {
				
				if (JAXBElement.class.isInstance(anyObj) && TInstanceDescriptors.class.isInstance(((JAXBElement) anyObj).getValue())) {
					TInstanceDescriptors instanceDescriptors = ((JAXBElement<TInstanceDescriptors>) anyObj).getValue();
					for (TInstanceDescriptor instanceDescriptor : instanceDescriptors.getInstanceDescriptor()) {
						if (instanceUri.equals(instanceDescriptor.getInstanceURI())) {
							return instanceDescriptor;
						}
					}
				}
			}
		}
		// System.out.println("Instance descriptor for the resource with the URI could not be found" + instanceUri);
		return null;
	}
	
	@Override
	public QName getResourceDefinitionBasedOnInstanceUri(String instanceUri) {
		for (TNodeTemplate nodeTemplate : this.getAdditionalGitHubRepositoriesToBeCrawled().getNodeTemplate()) {
			for (Object anyObj : nodeTemplate.getAny()) {
				
				if (JAXBElement.class.isInstance(anyObj) && TInstanceDescriptors.class.isInstance(((JAXBElement) anyObj).getValue())) {
					TInstanceDescriptors instanceDescriptors = ((JAXBElement<TInstanceDescriptors>) anyObj).getValue();
					for (TInstanceDescriptor instanceDescriptor : instanceDescriptors.getInstanceDescriptor()) {
						if (instanceUri.equals(instanceDescriptor.getInstanceURI())) {
							return nodeTemplate.getType();
						}
					}
				}
			}
		}
		System.err.println("Resource definition for the resource with the URI could not be found" + instanceUri);
		return null;
	}
	
	@Override
	public List<TInstanceDescriptor> getInstanceDescriptorsOfAResourceDomain(String instanceUri) {
		
		List<TInstanceDescriptor> instanceDescriptors = new ArrayList<TInstanceDescriptor>();
		for (TNodeTemplate nodeTemplate : this.getAdditionalGitHubRepositoriesToBeCrawled().getNodeTemplate()) {
			for (Object anyObj : nodeTemplate.getAny()) {
				// searched type instance then add types
				if (nodeTemplate.getType().getNamespaceURI().equals(instanceUri) && JAXBElement.class.isInstance(anyObj) && TInstanceDescriptors.class.isInstance(((JAXBElement) anyObj).getValue())) {
					TInstanceDescriptors containedInstanceDescriptors = ((JAXBElement<TInstanceDescriptors>) anyObj).getValue();
					for (TInstanceDescriptor instanceDescriptor : containedInstanceDescriptors.getInstanceDescriptor()) {
						instanceDescriptors.add(instanceDescriptor);
					}
				}
			}
		}
		return instanceDescriptors;
	}
	
	@Override
	public List<TInstanceDescriptor> getInstanceDescriptorsOfAResourceDefintion(QName resourceDefinitionId) {
		
		List<TInstanceDescriptor> instanceDescriptors = new ArrayList<TInstanceDescriptor>();
		for (TNodeTemplate nodeTemplate : this.getAdditionalGitHubRepositoriesToBeCrawled().getNodeTemplate()) {
			for (Object anyObj : nodeTemplate.getAny()) {
				// searched type instance then add types
				if (nodeTemplate.getType().equals(resourceDefinitionId) && JAXBElement.class.isInstance(anyObj) && TInstanceDescriptors.class.isInstance(((JAXBElement) anyObj).getValue())) {
					TInstanceDescriptors containedInstanceDescriptors = ((JAXBElement<TInstanceDescriptors>) anyObj).getValue();
					for (TInstanceDescriptor instanceDescriptor : containedInstanceDescriptors.getInstanceDescriptor()) {
						instanceDescriptors.add(instanceDescriptor);
					}
				}
			}
		}
		return instanceDescriptors;
	}
	
}
