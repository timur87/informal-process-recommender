package uni_stuttgart.ipsm.ipe_recommender.utils.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TCapabilityDefinition;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.resource.TAvailableCapabilityDefinitions;
import de.uni_stuttgart.resource.TAvailableResourceDefinitions;

import uni_stuttgart.ipsm.ipe_recommender.utils.ICapabilityReader;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
public class CapabilityReader implements ICapabilityReader {

    String fileName;
    TAvailableResourceDefinitions availableResourceDefinitions;
    TAvailableCapabilityDefinitions availableCapabilityDefinitions;
    IInformalProcessDataStoreAccess xmlFileNameReader;

    @Autowired
    public CapabilityReader(IInformalProcessDataStoreAccess reader) {
        setAvailableCapabilityDefinitions(reader.getAvailableCapabilityDefinitions());
        setAvailableResourceDefinitions(reader.getAvailableResourceDefinitions());
    }


    @Override
    public List<QName> findCapabilityTypesForNodeType(QName nodeType) {
        // List of capabilities available for the node type
        List<QName> listOfCapabilitiies = new ArrayList<QName>();
        for(TNodeType tempNodeType : getAvailableResourceDefinitions().getNodeType()){
            if(tempNodeType.getName().equals(nodeType.getLocalPart()) &&
               tempNodeType.getTargetNamespace().equals(nodeType.getNamespaceURI()) &&
               tempNodeType.getCapabilityDefinitions() != null){
                for(TCapabilityDefinition capDefinition : tempNodeType.getCapabilityDefinitions().getCapabilityDefinition()){
                    listOfCapabilitiies.add(capDefinition.getCapabilityType());
                }
                break;
            }
        }
        return listOfCapabilitiies;

    }

    /**
     * @return the availableResourceDefinitions
     */
    public TAvailableResourceDefinitions getAvailableResourceDefinitions() {
        return availableResourceDefinitions;
    }

    /**
     * @param availableResourceDefinitions the availableResourceDefinitions to set
     */
    public void setAvailableResourceDefinitions(TAvailableResourceDefinitions availableResourceDefinitions) {
        this.availableResourceDefinitions = availableResourceDefinitions;
    }

    /**
     * @return the availableCapabilityDefinitions
     */
    public TAvailableCapabilityDefinitions getAvailableCapabilityDefinitions() {
        return availableCapabilityDefinitions;
    }

    /**
     * @param availableCapabilityDefinitions the availableCapabilityDefinitions to set
     */
    public void setAvailableCapabilityDefinitions(TAvailableCapabilityDefinitions availableCapabilityDefinitions) {
        this.availableCapabilityDefinitions = availableCapabilityDefinitions;
    }

}
