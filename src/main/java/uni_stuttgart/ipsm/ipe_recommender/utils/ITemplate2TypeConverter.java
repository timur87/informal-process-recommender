package uni_stuttgart.ipsm.ipe_recommender.utils;

import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;

/**
 * This interface defines classes which
 *  converts between tNodeType, tCapabilityType <-> tNodeTemplate, tCapabilityTemplate
 */
public interface ITemplate2TypeConverter {
	TNodeType tNodeTemplate2tNodeType(TNodeTemplate nodeTemplate);
}
