package uni_stuttgart.ipsm.ipe_recommender;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import de.uni_stuttgart.iaas.ipsm.v0.ObjectFactory;
import de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationships;
import uni_stuttgart.ipsm.ipe_recommender.apis.InformalProcessRecommenderApi;

public class TTest {

	String[] listOfInputFileNames = {"InformalProcessDefinition1", "InformalProcessDefinition2", "InformalProcessDefinition3", "InformalProcessDefinition4", "InformalProcessDefinition5", "InformalProcessDefinition6", "InformalProcessDefinition7"};
	
	
	@Test
	public void generateRelevanceRelationships() {
		/*Initialize a recommender object*/
		InformalProcessRecommenderApi iprs = new InformalProcessRecommenderSpringBased();
		try {

			for (String fileName : this.listOfInputFileNames) {
				InputStream fileInputStream = this.getClass().getClassLoader().getResourceAsStream(fileName + ".xml");
				JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				/*Get informal process instances*/
				JAXBElement<TInformalProcessDefinition> processInstanceJaxb = (JAXBElement<TInformalProcessDefinition>) unmarshaller.unmarshal(fileInputStream);

				TInformalProcessDefinition informalProcessDefinition = processInstanceJaxb.getValue();

				List<TRelevanceRelationship> relevanceRelationships = iprs.getRelevanceRelationshipsForInformalProcessInstances(informalProcessDefinition, new ArrayList<TInformalProcessDefinition>());

				TRelevanceRelationships relevanceRelationshipsJaxb = new TRelevanceRelationships();
				relevanceRelationshipsJaxb.getRelevanceRelationship().addAll(relevanceRelationships);
				ObjectFactory objFactory = new ObjectFactory();
				Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
				marshaller.marshal(objFactory.createRelevanceRelationships(relevanceRelationshipsJaxb), new File("ResultsOf" + fileName + ".xml"));

				TInformalProcessDefinition recommendationModel = iprs.getRecommendationForInformalProcessInstancesUsingRelavanceRelationships(informalProcessDefinition, relevanceRelationships);
				marshaller.marshal(objFactory.createInformalProcessDefinition(recommendationModel), new File("RecommendationModelOf" + fileName + ".xml"));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
